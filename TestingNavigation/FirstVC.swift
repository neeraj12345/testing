//
//  FirstVC.swift
//  TestingNavigation
//
//  Created by Neeraj Chawla on 21/02/19.
//  Copyright © 2019 Neeraj Chawla. All rights reserved.
//

import UIKit

class FirstVC: UIViewController {

    
    var callBAck: (()-> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    deinit {
        print("First")
    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
            vc.callBAck = self.callBAck
           self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
