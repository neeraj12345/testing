//
//  SecondViewController.swift
//  TestingNavigation
//
//  Created by Neeraj Chawla on 21/02/19.
//  Copyright © 2019 Neeraj Chawla. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    
    
    var callBAck: (()-> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
 
    deinit {
        print("Second")
    }

    @IBAction func buttonAction(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: { [weak self] in
            guard let self = self else {return}
            self.callBAck?()
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
